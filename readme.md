# Spring Boot Config Server

## Objectives

* Setup Spring Boot Config Server
* Pull configurations from a git repository
* Authenticate to git repository using username/password
* Protect git repository password with encryption
* Protect access to Spring Boot Config Server with basic authentication
* Store secrets in git repository encrypted
* Optionally: protect access to Spring Boot Config Server with AD/Ldap credentials

## MVP

### Overview 

* Two spring boot config servers will be leveraged to separate the function of creating a cipher for credentials and decrypting the credentials by the application 
* The provisioning spring boot config server will be responsible for encrypting the secrets and does not require an authentication of any kind 
* The operational spring boot config server will require basic authentication to dispense clear text credentials to the client application. Client application will be provisioned during a deployment process leveraging environment variables 
![](./doc/SpringBootConfig.jpg)

### Credentials Provisioning 

* A Spring Boot Config Server will be deployed with /encrypt end-point enabled only, /decrypt or GET configuration end-points will be disabled to prevent credentials exposure. 
* A symmetrical key will be stored in the Spring Boot Config Server environment variable. 
* There is no authentication required to make a call to the server to encrypt any string. 
* Credentials from the system owner can be requested by DevOps or by anyone else 
![](./doc/ConfigMngProvisioning.png)

### Credentials Operations 

* A separate Spring Boot Config Server will be deployed with GET / configurations end-points enabled only. 
* An identical symmetrical key will be stored in the Spring Boot Config Server environment variable to decrypt the values encrypted during credential provisioning. 
* A set of basic authentication credentials will be provisioned to the client application to connect to the Spring Boot Config Server to get a clear text credentials/configurations 
![](./doc/ConfigMngOperations.png)

## The CloudIDE

1. This repository has been authored and tested using Cloud9 IDE as PoC for an isolated environment
2. All the dependencies have been installed and encapsulated in the setup.sh script

## Development Setup on CentOS (tested on Cloud9 IDE)

1. [Original post](http://www.baeldung.com/spring-cloud-configuration)
1. Clone the repository
1. `cd` into the repository folder
1. Run the setup script:
```
chmod +x ./setup.sh && \
./setup.sh
```

## Where are the configurations stored?

* Configuration are stored in a git [repository](../spring-boot-config-server-settings/readme.md)
* A sample configuration file: [customerprofile.properties](../../../spring-boot-config-server-settings/src/master/customerprofile.properties) in the master branch:
```
# Sample configuration settings
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/catalog
spring.datasource.username=dummy
spring.datasource.password={cipher}35f4bc9d7465633b929c0dbe973deabe35701497bbfdefc56663e77785e644ee361ee7437625ba8c971cb071a2b1466f
```

* Configurations can be stored in [multiple repos](https://cloud.spring.io/spring-cloud-config/single/spring-cloud-config.html#_pattern_matching_and_multiple_repositories) as well

## Where the secret is stored?

* Export environment variable: `export ENCRYPT_KEY=some-strong-secret`
* The encrypted variables can be stored in the [application.properties](./src/main/resources/application.properties) and inside the *.properties files in the settings repo:
* `spring.datasource.password={cipher}35f4bc9d7465633b929c0dbe973deabe35701497bbfdefc56663e77785e644ee361ee7437625ba8c971cb071a2b1466f`

## How to encrypt the secrets?

* By POSTing the clear-text string to the /ecnrpyt end-point the string to be encrypted with basic authentication, e.g.:
`curl -v -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8888/encrypt' --data-urlencode dummyStringToEncrypt`

## How the secrets are decrypted?

* Using the $ENCRYPT_KEY enviroment variables config server will decrypt the encrypted values automatically and return to the caller

## To run the service
```
./mvnw spring-boot:run
```

## To package the service:
```
./mvnw clean package
```

## To run the jar:
```
java -jar ./target/spring-boot-config-server-0.1.0.jar
```

## To test the end-point

* To fetch the config: `curl -v -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' localhost:8888/customerprofile/master`
* To encrypt a value: `curl -v -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8888/encrypt' --data-urlencode dummyStringToEncrypt`
* To encrypt/decrypt a value: 
```
encryptedValue=$(curl -s -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8080/encrypt' --data-urlencode 'dummyStringToEncrypt') && \
  echo $encryptedValue && \
  curl -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8888/decrypt' --data-urlencode $encryptedValue && \
  echo ""
```

## To deploy the service to pcf:

```
cf login -a api.run.pivotal.io
cf push -n spring-boot-config-server -p ./target/spring-boot-config-server-0.1.0.jar
```

**Don't forget** to setup an environment variable in Pcf for `ENCRYPT_KEY`

## Controlling the Service behaviour

* Check [application.properties](./src/main/resources/application.properties) comments to enable/disable features/end-points

## To make use of the config server

[Sample config client](../../../spring-boot-config-client)
